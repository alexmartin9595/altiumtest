﻿using AltiumTest.Shared;
using AltiumTest.Shared.Constants;

Console.WriteLine("Please, enter the file size (MB):");
int fileSize;

while (true)
{    
    try
    {
        fileSize = int.Parse(Console.ReadLine());
        if (fileSize > 0)
        {
            break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine("Please, enter valid value (from 1 to 100000):");
    }    
}


Console.WriteLine("Start new file generation");

DataGenerator dataGenerator = new DataGenerator(fileSize);
dataGenerator.Generate();

Console.WriteLine($"The file {AppConstants.SOURCE_FILE_PATH} was generated successfully");
Console.Read();
