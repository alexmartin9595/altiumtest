﻿using AltiumTest.Shared.Constants;
using AltiumTest.Shared.Helpers;

namespace AltiumTest.Shared
{
    public class DataSorter
    {
        private readonly FileHelper _fileHelper;

        public DataSorter ()
        {
            _fileHelper = new FileHelper();
        }

        public void Sort()
        {
            if (!File.Exists(AppConstants.SOURCE_FILE_PATH))
            {
                throw new Exception("File not exists");
            }

            var chunks = _fileHelper.SplitFile(AppConstants.SOURCE_FILE_PATH, 100000);
            _fileHelper.SortChunks(chunks);
            var result = _fileHelper.MergeChunks(chunks);
            _fileHelper.MoveToOutputFile(result, AppConstants.OUTPUT_FILE_PATH);
        }        
    }
}
