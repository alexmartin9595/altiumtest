﻿using AltiumTest.Shared.Constants;
using AltiumTest.Shared.Helpers;

namespace AltiumTest.Shared
{
    public class DataGenerator
    {
        private int _filesCount;

        private readonly FileHelper _helper;

        public DataGenerator (int fileSize)
        {
            _helper = new FileHelper();
            _filesCount = fileSize / AppConstants.TEMP_FILE_SIZE;
        }

        public void Generate()
        {
            Parallel.For(0, _filesCount, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
            {
                _helper.CreateTempFile(i);
            });

            _helper.MergeTempFiles(AppConstants.SOURCE_FILE_PATH, _filesCount);
        }
    }
}
