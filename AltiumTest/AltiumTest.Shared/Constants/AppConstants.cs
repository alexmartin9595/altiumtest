﻿namespace AltiumTest.Shared.Constants
{
    public static class AppConstants
    {
        public const string SOURCE_FILE_PATH = @"C:\testfile.txt";
        public const string OUTPUT_FILE_PATH = @"C:\sortedTestfile.txt";
        public const int TEMP_FILE_SIZE = 10;
    }
}
