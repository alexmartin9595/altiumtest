﻿namespace AltiumTest.Shared.Helpers
{
    public class SortHelper
    {
        private DataComparer _dataComparer;

        public SortHelper()
        {
            _dataComparer = new DataComparer();
        }

        public void Sort(string[] array)
        {
            Array.Sort(array, _dataComparer.Compare);
        }

        public int Compare(string line1, string line2)
        {
            return _dataComparer.Compare(line1, line2);
        }
    }
}
