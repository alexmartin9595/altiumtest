﻿using Bogus;

namespace AltiumTest.Shared.Helpers
{
    public class TextHelper
    {
        private const int SENTENCE_SIZE = 10;
        private const int MAX_NUMBER = 100000;

        private string[] _words;
        private Random _random;
        private readonly Faker _faker;

        public TextHelper()
        {
            _faker = new Faker("en");
            var text = _faker.Lorem.Sentence(SENTENCE_SIZE);
            _words = text.Split(" ");
            _random = new Random();
        }

        public string BuildLine()
        {
            int number = _random.Next(0, MAX_NUMBER);

            var startIndex = _random.Next(0, _words.Length - 2);
            var endIndex = _random.Next(startIndex + 1, _words.Length - 1);

            var updatedWords = _words.Skip(startIndex).Take(endIndex - startIndex).ToArray();
            return $"{number}. {string.Join(" ", updatedWords)}\n";
        }
    }
}
