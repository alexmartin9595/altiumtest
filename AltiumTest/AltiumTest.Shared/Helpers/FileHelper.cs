﻿using AltiumTest.Shared.Constants;

namespace AltiumTest.Shared.Helpers
{
    public class FileHelper
    {
        private int _tempFileSize;
        private readonly TextHelper _helper;
        private readonly SortHelper _sortHelper;

        public FileHelper()
        {
            _tempFileSize = AppConstants.TEMP_FILE_SIZE * 1024 * 1024;
            _helper = new TextHelper();
            _sortHelper = new SortHelper();
        }

        public void CreateTempFile(int fileNumber)
        {
            var tempFilePath = $"{fileNumber}.txt";

            if (File.Exists(tempFilePath))
            {
                File.WriteAllText(tempFilePath, string.Empty);
            }

            using (FileStream fs = File.Exists(tempFilePath) ? File.Open(tempFilePath, FileMode.Open) : new FileStream(tempFilePath, FileMode.CreateNew))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    while (sw.BaseStream.Length < _tempFileSize)
                    {
                        sw.Write(_helper.BuildLine());
                    }
                }
            }
        }

        public void MergeTempFiles(string outputFilePath, int filesCount)
        {
            if (File.Exists(outputFilePath))
            {
                File.WriteAllText(outputFilePath, string.Empty);
            }

            using (var outputStream = File.Exists(outputFilePath) ? File.Open(outputFilePath, FileMode.Open) : File.Create(outputFilePath))
            {
                for (var i = 0; i < filesCount; i++)
                {
                    using (var inputStream = File.OpenRead($"{i}.txt"))
                    {
                        inputStream.CopyTo(outputStream);
                    }

                    File.Delete($"{i}.txt");
                }
            }
        }
        public List<string> SplitFile(string filePath, int chunkSize)
        {
            var chunks = new List<string>();

            using (var reader = new StreamReader(filePath))
            {
                var chunk = new List<string>(chunkSize);
                while (!reader.EndOfStream)
                {
                    chunk.Add(reader.ReadLine());
                    if (chunk.Count == chunkSize)
                    {
                        var fileName = GenerateFileName();
                        WriteChunk(chunk, fileName);
                        chunks.Add(fileName);
                        chunk.Clear();
                    }
                }

                if (chunk.Count > 0)
                {
                    var fileName = GenerateFileName();
                    WriteChunk(chunk, fileName);
                    chunks.Add(fileName);
                }
            }

            return chunks;
        }

        public void WriteChunk(List<string> chunk, string fileName)
        {
            using (var writer = new StreamWriter(fileName))
            {
                foreach (var line in chunk)
                {
                    writer.WriteLine(line);
                }
            }
        }

        public void SortChunks(List<string> chunkFiles)
        {
            Parallel.ForEach(chunkFiles, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, chunkFile =>
            {
                var lines = File.ReadAllLines(chunkFile);
                _sortHelper.Sort(lines);
                File.WriteAllLines(chunkFile, lines);           
            });
        }

        public string MergeChunks(List<string> chunkFiles)
        {
            while (chunkFiles.Count > 1)
            {
                var newChunkFiles = new List<string>();
                var count = chunkFiles.Count;

                Parallel.For(0, count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                {
                    if (i % 2 == 1)
                    {
                        return;
                    }

                    string filePath = chunkFiles[i];
                    if (i < count - 1)
                    {
                        var mergedFile = MergeFiles(chunkFiles[i], chunkFiles[i + 1]);
                        filePath = mergedFile;
                    }

                    newChunkFiles.Add(filePath);
                });

                chunkFiles = newChunkFiles;
            }

            return chunkFiles[0];
        }        

        public void MoveToOutputFile(string sourcefileName, string outputFileName)
        {
            if (File.Exists(outputFileName))
            {
                File.WriteAllText(outputFileName, string.Empty);
            }

            using (var outputStream = File.Exists(outputFileName) ? File.Open(outputFileName, FileMode.Open) : File.Create(outputFileName))
            {
                using (var inputStream = File.OpenRead(sourcefileName))
                {
                    inputStream.CopyTo(outputStream);
                }
            }

            File.Delete(sourcefileName);
        }

        private string MergeFiles(string file1, string file2)
        {
            var mergedFile = GenerateFileName();

            using (var writer = new StreamWriter(mergedFile))
            {
                using (var reader1 = new StreamReader(file1))
                {
                    using (var reader2 = new StreamReader(file2))
                    {
                        string line1 = reader1.ReadLine();
                        string line2 = reader2.ReadLine();
                        while (line1 != null && line2 != null)
                        {
                            if (_sortHelper.Compare(line1, line2) < 0)
                            {
                                writer.WriteLine(line1);
                                line1 = reader1.ReadLine();
                            }
                            else
                            {
                                writer.WriteLine(line2);
                                line2 = reader2.ReadLine();
                            }
                        }

                        while (line1 != null)
                        {
                            writer.WriteLine(line1);
                            line1 = reader1.ReadLine();
                        }

                        while (line2 != null)
                        {
                            writer.WriteLine(line2);
                            line2 = reader2.ReadLine();
                        }
                    }
                }
            }

            File.Delete(file1);
            File.Delete(file2);

            return mergedFile;
        }

        private string GenerateFileName()
        {
            return $"{Guid.NewGuid()}.txt";
        }
    }
}
