﻿
namespace AltiumTest.Shared.Helpers
{
    public class DataComparer : IComparer<string>
    {
        public int Compare(string? line1, string? line2)
        {         
            int index1 = line1.IndexOf('.');            
            var text1 = line1.Substring(index1 + 2);

            int index2 = line2.IndexOf('.');            
            var text2 = line2.Substring(index2 + 2);         

            if (text1 == text2)
            {
                int number1 = int.Parse(line1.Substring(0, index1));
                int number2 = int.Parse(line2.Substring(0, index2));

                return number1.CompareTo(number2);
            }

            return text1.CompareTo(text2);
        }
    }
}
