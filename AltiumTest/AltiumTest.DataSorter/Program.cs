﻿using AltiumTest.Shared;
using System.Diagnostics;

Console.WriteLine("Start sorting...");

var watch = Stopwatch.StartNew();
DataSorter dataSorter = new DataSorter();

try
{
    dataSorter.Sort();
}
catch (Exception ex)
{
    Console.WriteLine(ex.ToString());
}


watch.Stop();
Console.WriteLine($"Sorting was completed for {watch.Elapsed.TotalSeconds} seconds");
