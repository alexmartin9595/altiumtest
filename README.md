## Usage

1. Start `AltiumTest.DataGenerator` console app.
2. When you see `Please, enter the file size (MB):` in colsole enter the number from 1 to 100000
3. Wait until the message `The file C:\testfile.txt was generated successfully` appears
4. Start `AltiumTest.DataSorter` console app.
5. Wait until message `Sorting was completed` appears.
6. Check the files `C:\testfile.txt` (generated file) and `C:\sortedTestfile.txt` (sorted file)

